import { Component, OnInit, Input } from '@angular/core';
import { MapService } from '@core/services/map.service';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  constructor(private map: MapService, public sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.map.buildMap();
  }
}
