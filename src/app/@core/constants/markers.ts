// https://www.coordenadas.com.es/espana/pueblos-de-guipuzcoa/20/1
export const DEBABARRENA = [
    {
        town: 'Soraluze',
        location: [ -2.4125, 43.1746 ]
    },
    {
        town: 'Antzuola',
        location: [ -2.3699809496, 43.1041437792684 ]
    },
    {
        town: 'Mendaro',
        location: [ -2.3785784162, 43.2457849908884 ]
    },
    {
        town: 'Arrasate',
        location: [ -2.4841486565, 43.0745226631642 ]
    },
    {
        town: 'Antzuola',
        location: [ -2.3699809496, 43.1041437792684 ]
    }
];
